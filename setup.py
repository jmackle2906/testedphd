from setuptools import setup

setup(
    name='testedPhD',
    version='0.0.1',
    packages=['testFastqPreprocessing', 'fastqPreprocessing'],
    url='',
    license='',
    author='jamesmackle',
    author_email='',
    description=''
)
