import fastqPreprocessing.scrape_fastqc_for_overrepresented_seq as scrape
import fastqPreprocessing.create_agg_files_of_overrepresented_seqs as agg

# nextfile
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--inputDirectory', '-i',
                    help='input directory html files',
                    type=str,
                    default=os.getcwd())

parser.add_argument('--stringToFind', '-s',
                    help="what should the identifier substring be",
                    type=str,
                    default='R1')

args = parser.parse_args()

input_dir = args.inputDirectory
string_to_find = args.stringToFind

os.chdir(input_dir)

list_dir = agg.get_list_of_files(input_dir, string_to_find)
list_of_sequence_lists = agg.generate_list_of_sequence_lists(list_dir,input_dir)

aggregate_list = agg.aggregate_sequences(list_of_sequence_lists)
print("number of overrepresented sequences found: " + str(len(aggregate_list)))

agg.open_output_file_and_write(input_dir, string_to_find, aggregate_list)

