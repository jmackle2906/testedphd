import fastqPreprocessing.scrape_fastqc_for_overrepresented_seq as scrape
import os


def open_output_file_and_write(location, string_to_find, aggreagate_sequences_to_write_to_file):
    out_file_name = string_to_find + '_agg.txt'
    with open(location + '/' + out_file_name, 'w+') as opened_file:
        write_sequences(opened_file, aggreagate_sequences_to_write_to_file)


def write_sequences(opened_file, aggreagate_sequences_to_write_to_file):
    count = 0
    for sequence in aggreagate_sequences_to_write_to_file:
        opened_file.writelines(">"+str(count)+'\n')
        opened_file.writelines(sequence+'\n')
        count += 1


def get_list_of_files(input_dir, string_to_find):
    files = os.listdir(input_dir)
    list_of_files = map(choose_which_files, files, ([string_to_find]*len(files)))
    return list(filter(None,list_of_files))

def choose_which_files(filename, string_to_find):
    if filename == string_to_find + '_agg.txt':
        pass
    if filename.__contains__(string_to_find):
        return filename

def generate_list_of_sequences(filename_and_location):
    return scrape.return_list_from_file(filename_and_location)

def aggregate_sequences(list_of_sequence_lists):
    aggregate = set()
    for sequence_list in list_of_sequence_lists:
        aggregate = add_unique_sequences_aggregate(aggregate , sequence_list)
    return aggregate

def generate_list_of_sequence_lists(list_of_files ,path):
    files = add_path_to_list_of_files(list_of_files, path)
    out_list_of_lists = []
    for file in files:
        out_list_of_lists.append(generate_list_of_sequences(file))
    # return list(map(generate_list_of_sequences, files))
    return out_list_of_lists

def add_path_to_list_of_files(list_of_files, path):
    out = []
    for item in list_of_files:
        if item.__contains__("_agg.txt"):
            pass
        else:
            item_with_path = path + '/' + item
            out.append(item_with_path)
    return out

def add_unique_sequences_aggregate(aggregate, sequences_to_add):
    aggregate = set(aggregate)
    for sequence in sequences_to_add:
        aggregate.add(sequence)
    return aggregate

def main_for_use(sequences_to_add):
    return aggregate_sequences(sequences_to_add)
