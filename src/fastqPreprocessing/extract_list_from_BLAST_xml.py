import bs4
from bs4 import BeautifulSoup
import sys
import lxml
# with open ("/media/james/500GB/PhD/2021/2021006K/human/Human_agg.txt") as file:
#     results= NCBIWWW.qblast("blastn", "nt", file.read())
#
#
#     print(results)
# blastResults=
fileName="8FABH7HN016-Alignment.xml"
# Directory="/media/james/500GB/PhD/2021/2021009k/separate/fastsqcTwo/"
infile=sys.argv[1]
outfile=infile+"TopBlastOrganisms.txt"



with open (infile, "r") as xml:
    readxml = xml.read()
    # tree=et.parse(readxml)
    # root=tree.getroot()
    # print(root)
Bs_data = BeautifulSoup(readxml, "xml")
iteration_hits = Bs_data.find_all("Iteration_hits")
top_hits=[]
count = 0
with open (outfile, "w+") as out:

    for it_hits in iteration_hits:
        try:
            hits = it_hits.find_all("Hit")
            hit_def = hits[0].find_all("Hit_def")
            # print(hit_def)
            outString = str(">"+str(count)+": " + str(hit_def[0])+"\n")
            # print(outString, end="")
            out.write(outString)
            count += 1
        except Exception:
            print("one of these threw an error")
            count += 1

            pass
        # top_hits.append([count, hits[0]])
        # out.write()

# print(top_hits)


# hdiff = hit_def.find_all("Hit_def")
# print (hit_def[0])