from bs4 import BeautifulSoup as bs


def make_soup(file_location):
    file = open(file_location)
    soup = bs(file, features="html.parser")
    file.close()
    return soup

def find_id_and_modules(soup):
    sequence_module = soup.find(id="M9")
    sequence_module_html = sequence_module.next_sibling
    sequence_module_table = sequence_module_html.find_all("td")
    return sequence_module_table[0::4]

def get_text(element):
    return element.text

def remove_html_tag(list_to_clean):
    return map(get_text, list_to_clean)

    # cleaned_list = []
    # for string_to_clean in list_to_clean:
    #     string_to_clean= string_to_clean.text
    #     cleaned_list.append(string_to_clean)
    # return cleaned_list

# def write_results_to_file_numbered(cleaned_list_of_sequences, output_file_name):
#     line_count = 0
#     with open(output_file_name,'w+') as out:
#         for sequence_to_write in cleaned_list_of_sequences:
#             out.writelines(">" + str(line_count) + "\n" + sequence_to_write + "\n")
#             line_count += 1


def return_list_from_file(input_file):
    file = make_soup(input_file)
    list_of_seqs_tags = find_id_and_modules(file)
    output_list_with_tags_removed = list(remove_html_tag(list_of_seqs_tags))
    return output_list_with_tags_removed


