import fastqPreprocessing.create_agg_files_of_overrepresented_seqs as toTest
import os

file_to_check = '/Users/jamesmackle/PhD/2022009K/firstfastqc/htmlOut/SRR2184207_R1_fastqc.html'
html_location = '/Users/jamesmackle/PhD/2022009K/firstfastqc/htmlOut'
string_to_find = 'R1'
specific_file_name = 'SRR2184199_R1_fastqc.html'
first_sequence = 'CGGATAAATAGTTTTTTGTATTTAAATTTCGTGGGAGGGAGAGTTAAATT'
agg_seqs = ['CGGTTTTAGAGGAATTTTGTTTTTGTGTGTTTTGAGTTTATTAGGTAGGT', 'CGGATAAATAGTTTTTTGTATTTAAATTTCGTGGGAGGGAGAGTTGAATT', 'CGGTTTTAGAGAAATTTTGTTTTCGTGTGTTTTAAGTTTATTAGGTAGTT', 'CGGATAAATAGTTTTTTGTATTTAAATTTTATGGGAGGGAGAGTTAAATT']

def test_open_output_file():
    toTest.open_output_file_and_write(html_location, string_to_find, agg_seqs)
    # print(html_location + '/' + string_to_find + '_agg.txt')
    assert os.path.isfile(html_location + '/' + string_to_find + '_agg.txt')

def test_get_list_of_files():
    list_of_files = list(toTest.get_list_of_files(html_location, string_to_find))
    assert len(list_of_files) > 2

def test_get_list_of_files_check_first():
    list_of_files = list(toTest.get_list_of_files(html_location, string_to_find))
    assert list_of_files[0] == specific_file_name


def test_generate_list_of_sequences():
    sequence_list = toTest.generate_list_of_sequences(html_location + '/' + specific_file_name)
    assert sequence_list[0] == first_sequence


def test_aggregate_sequences():
    list_of_files = list(toTest.get_list_of_files(html_location, string_to_find))
    list_of_sequence_lists = toTest.generate_list_of_sequence_lists(list_of_files,html_location)
    agg = toTest.aggregate_sequences(list_of_sequence_lists)
    assert list(agg).__contains__('CGGTGAGAGAGAGATTTAATCGTTTGGTTAGGTGGGTATTTTTGAGGTTG')

def test_scrape():
    assert len(toTest.scrape.return_list_from_file(file_to_check))>1

def test_main_for_use():

    list_dir = toTest.get_list_of_files(html_location, string_to_find)
    list_of_sequence_lists = toTest.generate_list_of_sequence_lists(list_dir, html_location)

    assert list(toTest.main_for_use(list_of_sequence_lists)).__contains__('CGGTGAGAGAGAGATTTAATCGTTTGGTTAGGTGGGTATTTTTGAGGTTG')