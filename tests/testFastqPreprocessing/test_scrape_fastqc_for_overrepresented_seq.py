import fastqPreprocessing.scrape_fastqc_for_overrepresented_seq as toTest
import os
import pytest

test_path = '/Users/jamesmackle/PhD/2022009K/firstfastqc/htmlOut'
file='SRR2184202_R1_fastqc.html'
file_path = test_path + '/' + file
# os.chdir(test_path)
remove_td_test_data = ['<td>CGGATAAATAGTTTTTTGTATTTAAATTTCGTGGGAGGGAGAGTTAAATT</td>',
                        '<td>CGGGAGTTTTAGTGTATTAGGGTTTTAGATGGTTTTTGGTTTTTTTTTTT</td>',
                        '<td>CGGTGAGAGAGAGATTTAATCGTTTGGTTAGGTGGGTATTTTTGAGGTTG</td>',
                        '<td>CGGATAAATAGTTTTTTGTATTTAAATTTCGTGGGAGGGAGAGTTGAATT</td>']

test_file_with_no_seqs = "/Users/jamesmackle/PhD/2022005K/fastqc/SRR11790875_1_fastqc.html"

output_file = 'fastqc_over_rep_seqs.txt'

def test_data_exists():
    assert os.path.exists(test_path)

def test_file_exists():
    assert os.path.isfile(file_path)

def test_make_soup():
    assert str(type(toTest.make_soup(file_path))) == "<class 'bs4.BeautifulSoup'>"

def test_find_id_and_modules():
    loaded_file = toTest.make_soup(file_path)
    output_of_test_find_id_and_modules = toTest.find_id_and_modules(loaded_file)
    assert len(output_of_test_find_id_and_modules) == 13

def test_file_that_has_no_overrepressented_sequences():
    loaded_file = toTest.make_soup(test_file_with_no_seqs)
    output_of_test_find_id_and_modules = toTest.find_id_and_modules(loaded_file)
    print(output_of_test_find_id_and_modules)
    assert len(output_of_test_find_id_and_modules) == 0

def test_remove_td_html_tag():
    loaded_file = toTest.make_soup(file_path)
    output_of_find_id_and_modules = toTest.find_id_and_modules(loaded_file)
    tds_removed = toTest.remove_html_tag(output_of_find_id_and_modules)
    print(tds_removed)
    for tds_removed_item in tds_removed:
        if str(tds_removed_item).__contains__('<td>'):
            pytest.fail()
        if str(tds_removed_item).__contains__('</td>'):
            pytest.fail()
        else:
            assert True

# def test_write_results_to_file_numbered():
#     loaded_file = toTest.make_soup(file_path)
#     output_of_find_id_and_modules = toTest.find_id_and_modules(loaded_file)
#     tds_removed = toTest.remove_html_tag(output_of_find_id_and_modules)
#     # tds_removed = toTest.remove_td_html_tag(remove_td_test_data)
#     os.chdir("/Users/jamesmackle/PycharmProjects/testedPhD/tests/testFastqPreprocessing/testData")
#     toTest.write_results_to_file_numbered(tds_removed, output_file)
#     assert os.path.isfile(output_file)